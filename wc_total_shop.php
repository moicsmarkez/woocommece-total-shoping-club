<?php
/** 
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the
 * plugin admin area. This file also defines a function that starts the plugin.
 *
 * @link               https://www.facebook.com/moicsmarkez
 * @since             0.8.9.1
 * @package       Wc_Total_Shop    
 *
 * @wordpress-plugin
 * Plugin Name:       Wc Total Shop
 * Plugin URI:          https://www.facebook.com/moicsmarkez
 * Description:       A plugin to configure shop on WooCommerce
 * Version:             0.8.9
 * Author:              Moics Markez
 * Author URI:        https://www.facebook.com/moicsmarkez
 * License:             GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:      wc-total-shop
 * Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'WC_total_Shop' ) ) :

/**
 * WC_total_Shop Class
 *
 * @class   WC_total_Shop
 * @since   0.8.9
 * @version 0.8.9
 */

final class WC_total_Shop {
    
    /**
	 * Plugin version.
	 *
	 * @var   string
	 * @since 0.8.9
	 */
	public $version = '0.8.9.1';
	/**
	 * @var   WC_total_Shop The single instance of the class
	 * @since 0.8.9
	 */
	protected static $instancia = null;

	/**
	 * Main WC_total_Shop Instance
	 *
	 * Ensures only one instance of WC_total_Shop is loaded or can be loaded.
	 *
	 * @version 0.8.9
	 * @since   0.8.9
	 * @static
	 * @return  WC_total_Shop - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$instancia ) ) {
			self::$instancia = new self();
		}
		return self::$instancia;
	}
	
	
	
	/**
	 * WC_total_Shop Constructor.
	 *
	 * @version 0.8.9.1
	 * @since   0.8.9.1
	 * @access  public
	 */
	function __construct() {
        // Register plugin text domain for translations files
		load_plugin_textdomain( 'wc-ss', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		// Include required files
		self::requires_incl();
    }
	
	
	/**
	 * Include core files used on the frontend.
	 *
	 * @version 0.8.9.1
	 * @since   0.8.9.1
	 */
	private static function requires_incl() {
		// Core
		include_once( plugin_dir_path( __FILE__ ) . 'shared/class-deserializer.php' );
        include_once( plugin_dir_path( __FILE__ ) . 'admin/menus/class-submenu.php' );
		// Include the dependencies needed to instantiate the plugin.
        foreach ( glob( plugin_dir_path( __FILE__ ) . 'admin/*.php' ) as $file ) {
            include_once $file;
        }
	}
   
		
    
}


endif;


if ( ! function_exists( 'single_class_wc_total_shop' ) ) {
	/**
	 * Returns the main instance of WC_total_Shop .
	 *
	 * @version 0.8.9.1
	 * @since   0.8.9.1
	 * @return  
	 */
	function single_class_wc_total_shop() {
		return WC_total_Shop::instance();
	}
}

single_class_wc_total_shop();
