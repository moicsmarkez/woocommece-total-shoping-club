<?php
/**
 * Retrieves information from the database.
 *
 * @package Wc_Total_Shop 
 */

/**
 * Retrieves information from the database.
 *
 * This requires the information being retrieved from the database should be
 * specified by an incoming key. If no key is specified or a value is not found
 * then an empty string will be returned.
 *
 * @package Wc_Total_Shop 
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'WCTS_Deserializer' ) ) :
 
 
class WCTS_Deserializer {


        protected static $instance = NULL;

        public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
        }

	/**
	 * Retrieves the value for the option identified by the specified key. If
	 * the option value doesn't exist, then an empty string will be returned.
	 *
	 * @param  string $option_key The key used to identify the option.
	 * @return string             The value of the option or an empty string.
	 */
	public function get_value( $option_key ) {
		return get_option( $option_key, '' );
	}
}
 
endif;

return WCTS_Deserializer::getInstance();
