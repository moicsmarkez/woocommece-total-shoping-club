<?php
/**
 * Retrieves information from the database.
 *
 * @package Wc_Total_Shop 
 */

/**
 * Retrieves information from the database.
 *
 * This requires the information being retrieved from the database should be
 * specified by an incoming key. If no key is specified or a value is not found
 * then an empty string will be returned.
 *
 * @package Wc_Total_Shop 
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'WCTS_Download' ) ) :
 
 
class WCTS_Download{

      function __construct(){
                add_action( 'wp_ajax_nopriv_stockCsv', array(__CLASS__,'make_csv_file'));
                add_action( 'wp_ajax_stockCsv', array(__CLASS__,'make_csv_file'));
                add_action('init', array(__CLASS__,'make_csv_file');
                add_action('admin_init', array(__CLASS__,'make_csv_file');
         }

    public static function make_csv_file(){
        echo 'ENTRE EN FUNCION';
        if(isset($_POST['downloadStockCsv']) && $_POST['downloadStockCsv'] == 'download'){
            echo 'ENTRE EN CONDICION DE FUNSION';
        //$stock = "";
        
        $array_to_csv = array();
        //First line
        $array_to_csv[] = array('id','sku','Product name','Manage stock','Stock status','Backorders','Stock','Type','Parent ID'); 
        
        $products = self::get_products_for_export(); 
        
        foreach( $products as $item ){ 
            $product_meta = get_post_meta($item->ID);
            $item_product = get_product($item->ID);
            $product_type = $item_product->product_type;
            $id = $item->ID;
            if(!empty($product_meta['_sku'][0])){          $sku = $product_meta['_sku'][0]; }else{ $sku = ''; }
            $product_name = $item->post_title;
            if(!empty($product_meta['_manage_stock'][0])){ $manage_stock = $product_meta['_manage_stock'][0]; }else{ $manage_stock = ''; }
            if(!empty($product_meta['_stock_status'][0])){ $stock_status = $product_meta['_stock_status'][0]; }else{ $stock_status = ''; }
            if(!empty($product_meta['_backorders'][0])){   $backorders = $product_meta['_backorders'][0]; }else{ $backorders = ''; }
            if(!empty($product_meta['_stock'][0])){        $stock = $product_meta['_stock'][0]; }else{ $stock = '0'; }
            if($product_type == 'variable'){               $product_type = 'variable'; }else{ $product_type = 'simple'; }
            
            $array_to_csv[] = array($id,$sku,$product_name,$manage_stock,$stock_status,$backorders,$stock,$product_type);
        
            if($product_type == 'variable'){
                        $args = array(
                        'post_parent' => $item->ID,
                        'post_type'   => 'product_variation', 
                        'numberposts' => -1,
                        'post_status' => 'publish' 
                        ); 
                        $variations_array = get_children( $args );
                        foreach($variations_array as $vars){
                    
                            $var_meta = get_post_meta($vars->ID);
                            $item_product = get_product($vars->ID);
            
                            $id = $vars->ID;
                            if(!empty($var_meta['_sku'][0])){          $sku = $var_meta['_sku'][0]; }else{ $sku = ''; }
                            $product_name = '';
                            foreach($item_product->variation_data as $k => $v){ 
                                $tag = get_term_by('slug', $v, str_replace('attribute_','',$k));
                                if($tag == false ){
                                    $product_name .= $v.' ';
                                }else{
                                    if(is_array($tag)){
                                        $product_name .= $tag['name'].' ';
                                    }else{
                                        $product_name .= $tag->name.' ';
                                    }
                                }
                            } 
                            
                            if(!empty($var_meta['_manage_stock'][0])){ $manage_stock = $var_meta['_manage_stock'][0]; }else{ $manage_stock = ''; }
                            if(!empty($var_meta['_stock_status'][0])){ $stock_status = $var_meta['_stock_status'][0]; }else{ $stock_status = ''; }
                            if(!empty($var_meta['_backorders'][0])){   $backorders = $var_meta['_backorders'][0]; }else{ $backorders = ''; }
                            if(!empty($var_meta['_stock'][0])){        $stock = $var_meta['_stock'][0]; }else{ $stock = '0'; }
                            $product_type = 'product-variant';
                            $parent_id = $item->ID; 
                            
                            $array_to_csv[] = array($id,$sku,$product_name,$manage_stock,$stock_status,$backorders,$stock,$product_type,$parent_id);                    
                        }
                }
            } 
            self::export_wcts_convert_to_csv($array_to_csv, 'mauna-es-export.csv', ',');
            
            //COLOCAR IMAGENES EN LA EXPORTACION
            // Export images/gallery
            /*
                if ( ! $export_columns || in_array( 'images', $export_columns ) ) {

                $image_file_names = array();

                // Featured image
                if ( ( $featured_image_id = get_post_thumbnail_id( $product->ID ) ) && ( $image = wp_get_attachment_image_src( $featured_image_id, 'full' ) ) ) {
                    $image_file_names[] = current( $image );
                }

                // Images
                $images  = isset( $meta_data['_product_image_gallery'][0] ) ? explode( ',', maybe_unserialize( maybe_unserialize( $meta_data['_product_image_gallery'][0] ) ) ) : false;
                $results = array();

                if ( $images ) {
                    foreach ( $images as $image_id ) {
                    if ( $featured_image_id == $image_id ) {
                        continue;
                    }
                    $image = wp_get_attachment_image_src( $image_id, 'full' );
                    if ( $image ) {
                        $image_file_names[] = current( $image );
                    }
                    }
                }

                $row[] = implode( ' | ', $image_file_names );

                }
                */
                //TERMINA EL SCRIPT PARA INCORPORAR IMAGENES!
                
        }
        
    }
    
    public static function get_products_for_export(){
  
        $args = array();
        $args['post_type'] = 'product';
        $args['posts_per_page'] = -1;
        
        $the_query = new WP_Query( $args );
        
        return $the_query->posts;
    }   
    
    public static function export_wcts_convert_to_csv($input_array, $output_file_name, $delimiter){
    
            $temp_memory = fopen('php://memory', 'w');
            
            foreach ($input_array as $line) {
                fputcsv($temp_memory, $line, $delimiter);
            }
            fseek($temp_memory, 0);
            
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
            
            fpassthru($temp_memory);
            exit();
        }

        
    /**
	 * Headers allready sent fix
	 *
	 */        
    public static function output_buffer() {
		ob_start();
    } 
	
}
 
endif;

return new WCTS_Download();
