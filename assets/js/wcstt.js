jQuery(document).ready(function() {
	// tab between them
    
    
        jQuery('#wcts-tabs-div-form').on('update-prices',  function(){
            one();
            jQuery('.accordion').find('.accordion-toggle').click(function(){
            
                //Expand or collapse this panel
                jQuery(this).next().slideToggle('fast');
            
                //Hide the other panels
                jQuery(".accordion-content").not(jQuery(this).next()).slideUp('fast');

            });
            //jQuery('.heading').hide();
            jQuery('.wcstt-ulcc').show();
        });
    
       jQuery('.wcstt-ulcc li a').each(function(i) {
               one();
            });
       
       function one(){
           jQuery('.wcstt-ulcc li a').each(function(i) {
                var thisTab = jQuery(this).parent().attr('class').replace(/active /, '');
                
                if ( 'active' != jQuery(this).attr('class') )
                    jQuery('div.' + thisTab).hide();
                
                jQuery('div.' + thisTab).addClass('tab-content');
        
                jQuery('div.'+thisTab+' > input.toggle_all').each(function(){
                if (jQuery(this).is(':checked')) {
                        jQuery('div.'+thisTab+' > div.accordion').hide();
                        jQuery('div.'+thisTab+' > div.all_variaciones_div').show();
                    } else {
                        jQuery('div.'+thisTab+' > div.accordion').show();
                        jQuery('div.'+thisTab+' > div.all_variaciones_div').hide();
                    }
            });
            
            //check todas las variaciones
            jQuery('div.'+thisTab+' > input.toggle_all').click(function(){
                
                //jQuery('.accordion').toggle(this.checked);
                if (this.checked) {
                    jQuery('div.'+thisTab+' > div.accordion').slideUp();
                    jQuery('div.'+thisTab+' > div.all_variaciones_div').slideDown();
                    } else {
                    jQuery('div.'+thisTab+' > div.accordion').slideDown(); 
                    jQuery('div.'+thisTab+' > div.all_variaciones_div').slideUp();
                    }
            });
                
                jQuery(this).click(function(){
                    // hide all child content
                    jQuery(this).parent().parent().parent().children('div').hide();
        
                    // remove all active tabs
                    jQuery(this).parent().parent('ul').find('li.active').removeClass('active');
        
                    // show selected content
                    jQuery(this).parent().parent().parent().find('div.'+thisTab).show();
                
                    jQuery(this).parent().parent().parent().find('li.'+thisTab).addClass('active');
                
                });
            });
       }

	jQuery('.accordion').find('.accordion-toggle').click(function(){
	
	      //Expand or collapse this panel
	      jQuery(this).next().slideToggle('fast');
	
	      //Hide the other panels
	      jQuery(".accordion-content").not(jQuery(this).next()).slideUp('fast');

	 });
	 
	//jQuery('.heading').hide();
	jQuery('.wcstt-ulcc').show();
      
    
     jQuery('body').on('click', 'div.wcstt-cc #wcts_meta_price_save', function () {
        var data = jQuery('#wcts-tabs-div-form :input').serializeArray();

        var form = jQuery('div#wcts-tabs-div-form');
        var action = form.attr('action');
        var method = form.attr('method');

        jQuery.ajax({
            url: action,
            method: method,
            data: data,
        beforeSend: function() {
            console.log('Updating Field');
            jQuery(this).attr('disabled', 'disable');
            jQuery('div.wcstt-cc').block({message: null,overlayCSS: {background: '#e4e4e4',opacity: 0.6}});
        },
        success : function( response ) {
             console.log('Success ' );
            jQuery("#wcts-tabs-div-form").html(response.data);
        },
        }).done(function (data) {
            jQuery(this).removeAttr('disabled');
            form.trigger('update-prices');
            jQuery('div.wcstt-cc').unblock();
        })
    });
    
});
