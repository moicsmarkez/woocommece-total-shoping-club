<?php
/**
 * Wc Total Shop Submenu - Core Class
 *
 * @version 0.8.9
 * @since   0.8.9
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WCTS_submenu' ) ) :

include_once( plugin_dir_path( __FILE__ ) . 'class-submenu-page.php' );
 
 class WCTS_submenu{
 
   	 
	 public function __construct( ) {
		add_action('admin_menu', array(__CLASS__,'wcts_submenu_'),99);
	}

	public static function wcts_submenu_() {
            $clase =WCTS_submenu_page::getInstance();
            $mainpage = add_menu_page('Wc Total Shop', 
                                        'Wc Total Shop', 
                                        'manage_options', 
                                        'wc_total_shop_opciones', 
                                        array($clase,'wcts_submenu_page_'),
                                        plugin_dir_url( __FILE__ ) .'../../assets/images/cart.png', 
                                        25);          
        add_action('load-'.$mainpage, array($clase,'admin_add_help_tab'));
    }

     
 }
 endif;
 
 return new WCTS_submenu();
