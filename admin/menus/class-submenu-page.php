<?php
/**
 * Wc Total Shop submenu page - Core Class
 *
 * @version 0.8.9.1
 * @since   0.8.9.1
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WCTS_submenu_page' ) ) :
 
 class WCTS_submenu_page{
        protected static $instance = NULL;

        public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
        }
 
    function __construct(){
        add_action( 'admin_enqueue_scripts', array(__CLASS__, 'add_my_stylesheet'));
    }
	
	public static function wcts_submenu_page_() {
            include_once( plugin_dir_path( __FILE__ ) .'../views/settings.php' );
    }
	       
      public static  function post_category_meta_box($box ) {
        $defaults = array('taxonomy' => 'category');
        if ( !isset($box['args']) || !is_array($box['args']) )
            $args = array();
        else
            $args = $box['args'];
        extract( wp_parse_args($args, $defaults), EXTR_SKIP );
        $tax = get_taxonomy($taxonomy);
    
    //vark => removed the divs with the tabs for 'all' and 'most popular'
        ?>
        <div id="taxonomy-<?php echo $taxonomy; ?>" class="categorydiv">
    
            <div id="<?php echo $taxonomy; ?>-pop" class="tabs-panel" style="display: none;">
                <ul id="<?php echo $taxonomy; ?>checklist-pop" class="categorychecklist form-no-clear" >
                    <?php $popular_ids = wp_popular_terms_checklist($taxonomy); ?>
                </ul>
            </div>
    
            <div id="<?php echo $taxonomy; ?>-all" class="tabs-panel">
                <?php
                $name = ( $taxonomy == 'category' ) ? 'post_category' : 'tax_input[' .  $tax_class . ']';     //vark replaced $taxonomy with $tax_class
                echo "<input type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
                ?>
                <ul id="<?php echo $taxonomy; ?>checklist" class="list:<?php echo $taxonomy ?> categorychecklist form-no-clear">
        <?php    

                switch( $taxonomy ) {
                case 'roles': 
                    self::roles_checklist($tax_class, $checked_list);
                    break;
                default:  //product category or vtmin category...
                    /* $this->vtmin_build_checkbox_contents ($taxonomy, $tax_class, $checked_list);*/
                    break;
                }
                
        ?>  
                </ul>
            </div>
            
        <?php //wp-hidden-children div removed, no longer functions as/of WP3.5 ?>

        </div>
        <?php
    }

        public static function roles_checklist ($tax_class, $checked_list = NULL) { 
            $roles = get_editable_roles();
            $roles['notLoggedIn'] = array( 'name' => 'Not logged in (just visiting)' );
            
            foreach ($roles as $role => $info) {
                $name_translated = translate_user_role( $info['name'] );
                $output  = '<li id='.$role.'>' ;
                $output  .= '<label class="selectit">' ;
                $output  .= '<input id="'.$role.'_'.$tax_class.' " ';
                $output  .= 'type="checkbox" name="tax-input-' .  $tax_class . '[]" ';
                $output  .= 'value="'.$role.'" ';
                if ($checked_list) {
                    if (in_array($role, $checked_list)) {   //if cat_id is in previously checked_list       
                    $output  .= 'checked="checked"';
                    }               
                }
                $output  .= '>'; //end input statement
                $output  .= '&nbsp;' . $name_translated;
                $output  .= '</label>';
                
                $output  .= '</li>';
                echo $output ;
            }
            return;   
        } 
        
     public static  function admin_add_help_tab() {
     $screen = get_current_screen();
          $screen->add_help_tab( array(
        'id'	=> '_help_tab_about_us',
        'title'	=> __('Sobre Nosotros','wc-total-shop'),
        'content'	=> '',
        'callback'=> array(__CLASS__, 'help_about'),
    ) );
     
     $screen->add_help_tab( array(
        'id'	=> '_help_tab_minimun_amount',
        'title'	=> __('Monto Minimo','wc-total-shop'),
        'content'	=> '',
        'callback'=> array(__CLASS__, 'help_minimun_amount'),
    ) );
    
    $screen->add_help_tab( array(
        'id'	=> 'discount_role',
        'title'	=> __('Descuento por Rol','wc-total-shop'),
        'content'	=> '',
        'callback'=> array(__CLASS__, 'help_discount_role'),
    ) );
    
    $screen->add_help_tab( array(
        'id'	=> 'recarga_equivalencia',
        'title'	=> __('Impues adicional RE','wc-total-shop'),
        'content'	=> '',
        'callback'=> array(__CLASS__, 'help_recarga_equivalencia'),
    ) );
    
    $screen->add_help_tab( array(
        'id'	=> 'fixed_price',
        'title'	=> __('Precios Fijos','wc-total-shop'),
        'content'	=> '',
        'callback'=> array(__CLASS__, 'help_fixed_prices'),
    ) );
}
        
    public static function help_about() {
            include(plugin_dir_path( __FILE__ ) .'../helpers/about.php');
		}   
    
    public static function help_fixed_prices() {
            include(plugin_dir_path( __FILE__ ) .'../helpers/helper_fixed_price_role.php');
		}   
    
    public static function help_recarga_equivalencia() {
            include(plugin_dir_path( __FILE__ ) .'../helpers/helper_recarga_equivalencia.php');
		}   
    
    public static function help_discount_role() {
            include(plugin_dir_path( __FILE__ ) .'../helpers/helper_discount_role.php');
		}   
        
    public static function help_minimun_amount() {
            include(plugin_dir_path( __FILE__ ) .'../helpers/helper_minimun_amount.php');
		}
        
    public static function add_my_stylesheet() {
        if ( isset($_GET['page']) && $_GET['page'] == 'wc_total_shop_opciones' ) {
            wp_enqueue_style( 'settings-box', plugins_url( '../../assets/css/settings-box.css', __FILE__ ) );
            wp_enqueue_script( 'settings-box', plugins_url( '../../assets/js/settings-box.js',    __FILE__ ), array( 'jquery' ) );
        }
    }

	
 }
 endif;
 
 return WCTS_submenu_page::getInstance();
