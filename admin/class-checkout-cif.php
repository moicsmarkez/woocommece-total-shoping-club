<?php
/**
 * WC Total ShopMinimun Add fiel CIF on checkout Page for WooCommerce - Core Class
 *
 * @version 0.8.9.1
 * @since   0.8.9.1
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_CIF_checkout' ) ) :
 
 class WC_CIF_checkout{
    
     function __construct(){
                    add_action('woocommerce_after_checkout_billing_form',  array( __CLASS__, 'wcts_cif_checkout'));
                    add_action('woocommerce_checkout_process',  array( __CLASS__, 'wcts_cif_checkout_proc'));
                    add_action('woocommerce_checkout_update_order_meta',  array( __CLASS__, 'wcts_cif_checkout_update_order_meta'));
                    add_action('woocommerce_email_order_meta',  array( __CLASS__, 'wcts_cif_checkout_order_meta_keys'), 10, 3); //para correos 
                    add_action( 'woocommerce_admin_order_data_after_billing_address', array( __CLASS__, 'wcts_checkout_field_display_admin_order_meta'), 10, 1 );
                    add_action( 'woocommerce_order_details_after_customer_details', array(__CLASS__,'wcts_checkout_field_display_admin_order_meta'), 10, 1 );
         }
    
    private $concidencias=0;

    public static function wcts_cif_checkout( $checkout ) {
    $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-cif');
            $concidencias = 0;
             if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            if($concidencias > 0){
                    echo '<div id="wcts_cif_checkout"><h3>'.__('NIF/CIF').'</h3>';
                    
                    woocommerce_form_field( 'NIF', array(
                    'type' => 'text',
                    'class' => array('my-field-class form-row-wide'),
                    'label' => __('NIF/CIF'),
                    'placeholder' => __('Introduzca su NIF/CIF'),
                    'required'  => true,
                    ), $checkout->get_value( 'NIF' ));
                    
                    echo '</div>';
                }
    }

    public static function wcts_cif_checkout_proc() {
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-cif');
            $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
        
       if (!$_POST['NIF'] ){
            if($concidencias > 0 ){        
                    wc_add_notice( sprintf( __('Por favor introduce tu NIF o CIF.') ), 'error' );
                } 
            }
     }
    
 
    public static function wcts_cif_checkout_update_order_meta( $order_id ) {
        if ($_POST['NIF']) update_post_meta( $order_id, 'NIF/CIF', esc_attr($_POST['NIF']));
    }
 
 
 
 
 
    public static function wcts_cif_checkout_order_meta_keys( $order, $sent_to_admin, $plain_text  ) {
        $user_A = new WP_User( $order->user_id );   
        
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-cif');
        $concidencias = 0;
             if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, $user_A->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            
        if ($concidencias > 0 ){
            $CIF =  get_post_meta( $order->id, 'NIF/CIF', true )!='' ?  get_post_meta( $order->id, 'NIF/CIF', true ): '';
            if($plain_text){
                echo 'Número de Identificación Fiscal / Código de identificación fiscal '.$CIF;
            }else {
                echo '<h2 style="color:#a5842e;display:block;font-family:&quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:16px 0 8px;text-align:left;" >Datos de Fisales</h2>';
                echo '<ul ><li ><strong>NIF/CIF:</strong> <span  style="color:#505050;font-family:&quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;">'.$CIF.'</span></li></ul>';
			}
        }
    }
     
    public static function wcts_checkout_field_display_admin_order_meta($order){
        $user_A = new WP_User( $order->user_id );   
        
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-cif');
        $concidencias = 0;
             if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, $user_A->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
        $CIF =  get_post_meta( $order->id, 'NIF/CIF', true )!='' ?  get_post_meta( $order->id, 'NIF/CIF', true ): '';
        if($CIF !='' && $concidencias > 0 )
            echo '<p style="font-size: 16px;color: black;margin: 20px 0;" ><strong>'.__('NIF/CIF').': </strong> ' .$CIF. '</p>';
    }
  
    
  }
  
  
endif;

return new WC_CIF_checkout();

 
 
