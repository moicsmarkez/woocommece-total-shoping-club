<div id="wcst_help_instructions">

<h1><?php _e('WooCommerce Total Shop Instrucciones de Uso','wc-total-shop'); ?></h1>
<h2><?php _e('Monto minimo para comprar','wc-total-shop'); ?></h2>
<img style="width: 100%;" src="<?php echo plugin_dir_url( __FILE__ ) .'../../assets/images/help_minimun_amount.png'; ?>"  alt="Monto Minimo de Compra" />

<h2 style="color: #62a31e" ><?php _e('Paso 1:','wc-total-shop'); ?></h2>
<h3><?php _e('Selecciona los Roles','wc-total-shop'); ?></h3>
<p><?php _e('Selecciona el rol que deseas limitar en la compra.','wc-total-shop'); ?></p>

<h2 style="color: #c75757"><?php _e('Paso 2:','wc-total-shop'); ?></h2>
<h3><?php _e('Desliza hasta llegar al monto deseado','wc-total-shop'); ?></h3>
<p><?php _e('Desliza el slider hasta aumentar el monto especifico que deseas se pueda limitar la orden minima de compra.','wc-total-shop'); ?></p>


<h3><?php _e('Guardar Cambios','wc-total-shop'); ?></h3>
<p><?php _e('Guarda los cambios, acuerdates que puedes seleccionar mas de un rol para esta restrincion.','wc-total-shop'); ?></p>
<img style="width: 15%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/guardar.png'; ?>" alt="Guardar" />
<hr />
<h3><?php _e('¡Resultados!','wc-total-shop'); ?></h3>
<img style="width: 80%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/result_minimun_amount.png'; ?>" alt="Resultador" />
</div>
