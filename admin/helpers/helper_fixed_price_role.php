<div id="wcst_help_instructions">

<h1><?php _e('WooCommerce Total Shop Instrucciones de Uso','wc-total-shop'); ?></h1>
<h2><?php _e('Precios fijos a productos segun roles','wc-total-shop'); ?></h2>
<img style="width: 70%;" src="<?php echo plugin_dir_url( __FILE__ ) .'../../assets/images/help_fixed_price.png'; ?>"  alt="Descuentos porcentual" />
<img style="width: 70%;" src="<?php echo plugin_dir_url( __FILE__ ) .'../../assets/images/help_fixed_price_2.png'; ?>"  alt="Descuentos porcentual" />

<h2 style="color: #2f9200" ><?php _e('Paso 1:','wc-total-shop'); ?></h2>
<h3><?php _e('Selecciona los Roles','wc-total-shop'); ?></h3>
<p><?php _e('Selecciona el rol al cual deseas asignar un precio fijo por producto.','wc-total-shop'); ?></p>

<h2 style="color: #cd1b1b"><?php _e('Paso 2:','wc-total-shop'); ?></h2>
<h3><?php _e('Asigna el precio ajustado al rol','wc-total-shop'); ?></h3>
<p><?php _e('Buscar el rol que deseas se le modifique el precio.','wc-total-shop'); ?></p>
<p><?php _e('Ajusta el precio a tu atonjo (Precio normal, o precio con rebaja).','wc-total-shop'); ?></p>


<h3><?php _e('Guardar Cambios','wc-total-shop'); ?></h3>
<p><?php _e('Guarda los cambios, acuerdates que puedes seleccionar mas de un rol para esta restrincion.','wc-total-shop'); ?></p>
<img style="width: 15%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/guardar.png'; ?>" alt="Guardar" />
<hr />
<h3><?php _e('¡Resultados!','wc-total-shop'); ?></h3>
<img style="width: 65%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/result_fixed_price.png'; ?>" alt="Resultador" />

</div>
