<div id="wcst_help_instructions">

<h1><?php _e('WooCommerce Total Shop Instrucciones de Uso','wc-total-shop'); ?></h1>
<h2><?php _e('Impuesto Adicional recarga de equivalencias','wc-total-shop'); ?></h2>
<img style="width: 100%;" src="<?php echo plugin_dir_url( __FILE__ ) .'../../assets/images/help_recarga_equivalencia.png'; ?>"  alt="Monto Minimo de Compra" />

<h2><?php _e('Paso 0:','wc-total-shop'); ?></h2>
<?php _e('Antes de configurar estas opcion tienes que saber:','wc-total-shop'); ?>
<ul>
<li><?php _e('Este impuesto solo es apicable para ciudades en el pais de españa.','wc-total-shop'); ?></li>
<li><?php _e('El impuesto se calcula sobre el coste total del pedido.','wc-total-shop'); ?></li>
</ul>

<h2 style="color: #e0d50f" ><?php _e('Paso 1:','wc-total-shop'); ?></h2>
<h3><?php _e('Selecciona los Roles','wc-total-shop'); ?></h3>
<p><?php _e('Selecciona los roles que se les cobrara el impuesto adicional al momento de pagar.','wc-total-shop'); ?></p>

<h2 style="color: #2830e5"><?php _e('Paso 2:','wc-total-shop'); ?></h2>
<h3><?php _e('Aumentar o Disminuir Porcentaje','wc-total-shop'); ?></h3>
<p><?php _e('Manten precionado en los signos negativo (-) o positivo (+) para aumentar el porcentaje de cobro.','wc-total-shop'); ?></p>


<h3><?php _e('Guardar Cambios','wc-total-shop'); ?></h3>
<p><?php _e('Guarda los cambios, acuerdates que puedes seleccionar mas de un rol para esta restrincion.','wc-total-shop'); ?></p>
<img style="width: 15%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/guardar.png'; ?>" alt="Guardar" />
<hr />
<h3><?php _e('¡Resultados!','wc-total-shop'); ?></h3>
<img style="width: 50%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/result_recarga_equivalencia.png'; ?>" alt="Resultador" />
</div>
