<div id="wcst_help_instructions">

<h1><?php _e('WooCommerce Total Shop Instrucciones de Uso','wc-total-shop'); ?></h1>
<h2><?php _e('Descuento porcentual en todos los productos segun roles','wc-total-shop'); ?></h2>
<img style="width: 100%;" src="<?php echo plugin_dir_url( __FILE__ ) .'../../assets/images/help_discount_role.png'; ?>"  alt="Descuentos porcentual" />

<h2 style="color: #8f085e" ><?php _e('Paso 1:','wc-total-shop'); ?></h2>
<h3><?php _e('Selecciona los Roles','wc-total-shop'); ?></h3>
<p><?php _e('Selecciona el rol que deseas limitar en la compra.','wc-total-shop'); ?></p>

<h2 style="color: #127071"><?php _e('Paso 2:','wc-total-shop'); ?></h2>
<h3><?php _e('Desliza hasta llegar al porcentaje de descuento deseado','wc-total-shop'); ?></h3>
<p><?php _e('Desliza el slider hasta aumentar el porcentaje especifico que deseas utilizar como descuento a todos lo productos.','wc-total-shop'); ?></p>


<h3><?php _e('Guardar Cambios','wc-total-shop'); ?></h3>
<p><?php _e('Guarda los cambios, acuerdates que puedes seleccionar mas de un rol para esta restrincion.','wc-total-shop'); ?></p>
<img style="width: 15%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/guardar.png'; ?>" alt="Guardar" />
<hr />
<h3><?php _e('¡Resultados!','wc-total-shop'); ?></h3>
<img style="width: 65%;" src="<?php echo plugin_dir_url(__FILE__).'../../assets/images/result_discount_amount.png'; ?>" alt="Resultador" />

</div>
