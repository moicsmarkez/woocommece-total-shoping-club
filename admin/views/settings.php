
<div class="wrap">
            <h1 style="font-size: 30px;font-weight: 900;text-shadow: 3px 3px 5px #929292;" ><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <h3><?php  _e('Modifica las opciones para ser asignadas a los roles','wc-total-shop'); ?></h3>
            <?php do_action( 'get_wcts_settings_messages' ); ?>
            <form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>" >
            <div class="acordion">
                <h4 class="accordion-toggle"><?php _e('Compra por monto minimo','wc-total-shop'); ?> </h4>
                <div class="accordion-content" style="display: block;">
                    <p><?php  _e('Debes indicar el monto minimo segun tengas que condicionar la compra, e.g.: 150','wc-total-shop'); ?></p>
                     <div class="rango">
                        <div class="etuque"><?php echo esc_attr( get_woocommerce_currency_symbol()) ?> 1000</div>
                        <input id="slider_price1"  type="range" min="10" max="1000" step="5" value="<?php echo esc_attr(  WCTS_Deserializer::getInstance()->get_value('minimiun-amount') ) ?>" onfocus="printValue('slider_price1','valor_front1','rangeValue1')" orient="vertical"  />
                        <input id="rangeValue1" type="hidden" name="minimiun-amount" value="<?php echo esc_attr( WCTS_Deserializer::getInstance()->get_value('minimiun-amount') ) ?>"  readonly />
                        <div class="etuque"><?php echo esc_attr( get_woocommerce_currency_symbol()) ?> 10</div>
                    </div>
                    <div class="roles">
                        <div style="text-align: center;font-size: 24px;font-weight: 900;margin-bottom: 15px;">
                            <label><?php echo esc_attr( get_woocommerce_currency_symbol()) ?></label>
                            <label id="valor_front1"  ><?php echo esc_attr( WCTS_Deserializer::getInstance()->get_value('minimiun-amount')) ?></label>
                        </div>
                        <?php
                            // ********************************
                                self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-min', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-min')   )));
                            // ********************************
                        ?>
                    </div>
                </div>
                <h4 class="accordion-toggle"><?php  _e('Descuentos para todos los productos','wc-total-shop'); ?></h4>
                <div class="accordion-content">
                        <p><?php   _e('Debes indicar el porcentaje de descuento.','wc-total-shop'); ?></p>
                        <div class="rango">
                            <div class="etuque"> 100% </div>
                            <input id="slider_price2"  type="range" min="1" max="100" step="1" value="<?php echo esc_attr( WCTS_Deserializer::getInstance()->get_value('discount-pocentaje')) ?>" onfocus="printValue('slider_price2','valor_front2','rangeValue2')" orient="vertical"  />
                            <input id="rangeValue2" type="hidden" name="discount-pocentaje" value="<?php echo esc_attr(  WCTS_Deserializer::getInstance()->get_value('discount-pocentaje') ) ?>"  readonly />
                            <div class="etuque"> 1% </div>
                        </div>
                        <div class="roles">
                            <div style="text-align: center;font-size: 24px;font-weight: 900;margin-bottom: 15px;">
                                <label id="valor_front2"  ><?php echo esc_attr(  WCTS_Deserializer::getInstance()->get_value('discount-pocentaje') ) ?></label>
                                <label>%</label>
                            </div>
                        <?php
                        // ********************************
                        self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-dis', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-dis')   )));
                        // ********************************
                        ?>
                        </div>
                </div>
                <h4 class="accordion-toggle"><?php   _e('Impuesto de Recarga de Equivalencias','wc-total-shop'); ?></h4>
                <div class="accordion-content">
                <p><?php  _e('Debes indicar el porcentaje de impuesto para recargo de equvalencia a los roles seleccionados.','wc-total-shop'); ?></p>
                <div style="text-align: center;font-size: 24px;font-weight: 900;margin-bottom: 15px;">
                                <label id="wcts_tax_control_less" class="wcts_tax_control">-</label>
                                <label id="valor_front3"  ><?php echo  esc_attr( WCTS_Deserializer::getInstance()->get_value('re-tax-pocentaje') ) ?></label>
                                <label>%</label>
                                <label id="wcts_tax_control_more"  class="wcts_tax_control">+</label>
                </div>
                <input id="number_tax"  type="hidden" name="re-tax-pocentaje" value="<?php echo  esc_attr( WCTS_Deserializer::getInstance()->get_value('re-tax-pocentaje') ) ?>"   />
                <?php
                 // ********************************
                   self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-re-tax', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-re-tax')   )));
                // ** ******************************
                ?>
                </div>
                <h4 class="accordion-toggle"><?php _e('Precios fijos','wc-total-shop'); ?> </h4>
                <div class="accordion-content">
                <p style="margin-bottom: 30.5px;"><?php   _e('Debes indicar los roles seleccionados que tendran precio fijo por cada producto.','wc-total-shop'); ?></p>
                <br />
                <?php
                 // ********************************
                  self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-pr-fj', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-pr-fj')   )));
                // ** ******************************
                ?>
            </div>    
<!--             // *** Voy colocar aqui actualizacion Campo NIF CIF 23/05/2017 -->
                <h4 class="accordion-toggle"><?php _e('Código de Identificación Fiscal','wc-total-shop'); ?> </h4>
                <div class="accordion-content">
                <p style="margin-bottom: 30.5px;"><?php   _e('Debes indicar los roles seleccionados que tendran el campo de CIF en su Formulario de Facturación.','wc-total-shop'); ?></p>
                <br />
                <?php
                 // ********************************
                  self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-cif', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-cif')   )));
                // ** ******************************
                ?>
            </div>    
<!--             // ******** -->
<!--             // *** Voy colocar aqui actualizacion Exportar csv -->
                <h4 class="accordion-toggle"><?php _e('Exportar archivos CSV por role','wc-total-shop'); ?> </h4>
                <div class="accordion-content">
                <p style="margin-bottom: 30.5px;"><?php   _e('Debes indicar los roles seleccionados que tendran la posibilidad de descargar los archivos de exportar para su catalogo.','wc-total-shop'); ?></p>
                <br />
                <?php
                 // ********************************
                  self::post_category_meta_box(array( 'args' => array( 'taxonomy' => 'roles', 'tax_class' => 'role-exp', 'checked_list' => WCTS_Deserializer::getInstance()->get_value('tax-input-role-exp')   )));
                // ** ******************************
                ?>
            </div>    
<!--             // ******** -->
            </div><!-- Termina Acordion -->
            <script>
                function printValue(sliderID, labelbox, textbox) {
                    var x = document.getElementById(textbox);
                    var y = document.getElementById(sliderID);
                    var z = document.getElementById(labelbox);
                    y.addEventListener("input", function() {
                        z.innerHTML = y.value;
                        x.value = y.value;
                    }, false); 
                    
                }
                      var y = document.getElementById('wcts_tax_control_less');
                      y.addEventListener("mousedown", function() {
                        repiteEsto = setInterval(decrementa, 65);
                    }, false);
                    y.addEventListener("mouseup", function() {
                        clearInterval(repiteEsto);
                    }, false); 
                    function decrementa(){
                        var z = document.getElementById('number_tax');
                        var y = document.getElementById('valor_front3');
                        var t = parseFloat(z.value) - 0.01;
                        if(t <=1){
                            //nothing
                        }else{
                            z.value = t.toFixed(2);
                            y.innerHTML = t.toFixed(2);
                        }
                    }
                    var y = document.getElementById('wcts_tax_control_more');
                     y.addEventListener("mousedown", function() {
                        repiteEsto = setInterval(incrementa, 65);
                    }, false);
                    y.addEventListener("mouseup", function() {
                        clearInterval(repiteEsto);
                    }, false); 
                    function incrementa(){
                        var z = document.getElementById('number_tax');
                        var y = document.getElementById('valor_front3');
                        var t = parseFloat(z.value) + 0.01;
                        if( t > 100 ){
                          //nothing
                        }else {
                            z.value = t.toFixed(2);
                            y.innerHTML = t.toFixed(2);
                        }
                    }
            </script>
            <?php
                wp_nonce_field( 'wcts-settings-save', 'wcts-prior-shop' );
                @submit_button(); 
            ?>
            </form>
            </div><!-- .wrap -->
