<?php
/**
 * Wc Total Shop Metabox Products - Core Class
 *
 * @version 0.8.9
 * @since   0.8.9
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_total_metabox_product' ) ) :

  class WC_total_metabox_product{
    
        protected static $instance = NULL;

        public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
        }
  
    private static function cargaDatos($prod_id, $nombre){
        
            $post_id_ = get_post_custom($prod_id);
            $dato = isset($post_id_['wcts_'.$nombre][0]) ? esc_attr($post_id_['wcts_'.$nombre][0]) : null;
            
            return $dato;
        
        }

      /**
       * @param $role_checklist
       * @param $id
       * @param $prod
       * @param $product_type
       * @return string
       */
      public static function get_form_prices($role_checklist, $id, $prod, $product_type){
        $show = '<div class="wcstt-cc" >
                            <ul class="wcstt-ulcc" id="wcstt-ulcc">';
			    if ($role_checklist) {
                    $concidencias=0;
                    foreach (get_editable_roles() as $role => $info) {
                    $name_translated = translate_user_role( $info['name'] );
                        if (in_array($role, $role_checklist)) {
                            if(!$concidencias){
                                $show .= '<li class="active '.$role.'-'.$concidencias.'"><a class="active" href="javascript:void(null);">'.$name_translated.'</a></li>';
                            }else{
                                $show .='<li class="'.$role.'-'.$concidencias.'"><a href="javascript:void(null);">'.$name_translated.'</a></li>';
                            }
                            $concidencias++;
                        }
                    }
                }
			$show .='</ul>';
			if ($role_checklist) {
                $concidencias=0;
                    foreach (get_editable_roles() as $role => $info) {
                        if (in_array($role, $role_checklist)) {
                           $active = self::cargaDatos($id, str_replace("-", "_", $role).'_'.$id.'_todas_check_b') ;
                            $show .='<div class="'.$role.'-'.$concidencias.'" >
                                       <!-- <h4 class="heading"> Tab '.$concidencias.'</h4>-->';
                                    if($product_type == 'variable'){
                                        $show .='<input class="toggle_all" type="checkbox" id="'.str_replace("-", "_", $role).'_'.$id.'_todas_check_b" name="'.str_replace("-", "_", $role).'_'.$id.'_todas_check_b" value="checked" '.(empty($active)?'':$active).' />
                                            <span>'.__('Todas las variaciones','wc-total-shop').' </span>';
                                            $show .='<div class="accordion">';
                                            $args = array(
                                                'post_type'      => 'product_variation',
                                                'post_status'    => array( 'private', 'publish' ),
                                                'posts_per_page' => -1, 
                                                'orderby'        => array( 'menu_order' => 'ASC', 'ID' => 'DESC' ),
                                                'post_parent'    => $id,
                                                'fields' => 'ids'
                                            );
                                            $variations = get_children( $args );
                                            foreach($variations as $ids){
                                                $prod_ = wc_get_product($ids);
                                                $name = '#'.$ids;
                                                $name .= ' '.wc_get_formatted_variation($prod_-> get_attributes(),true);
                                                $show .= '<h4 class="accordion-toggle">'.$name.'</h4>';
                                                $show .= '<div class="accordion-content ">
                                                                <div>
                                                                    <table class="form-table">
                                                                        <tr>
                                                                            <th scope="row" ><span>'. __('Precio normal','wc-total-shop').' ('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input id="'.str_replace("-", "_", $role).'_'.$ids.'_retail" name="'.str_replace("-", "_", $role).'_'.$ids.'_retail" min="0" step="0.001"  type="number" value="'.self::cargaDatos($ids,str_replace("-", "_", $role).'_'.$ids.'_retail').'" /><span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row" ><span>'.__('Precio rebajado','wc-total-shop').' ('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input id="'.str_replace("-", "_", $role).'_'.$ids.'_sale" name="'.str_replace("-", "_", $role).'_'.$ids.'_sale" min="0" step="0.001"  type="number" value="'.self::cargaDatos($ids,str_replace("-", "_", $role).'_'.$ids.'_sale').'" /><span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                    </table>
                                                                    <span style="float: left;margin: 10px 0px 0px 0px;"><b><em>'. __(' Precio Normal establecido','wc-total-shop').': '.esc_attr( get_post_meta($ids,'_regular_price',true)).' '.get_woocommerce_currency_symbol().' </em></b>  </span>';
                                                                    if( get_post_meta($ids,'_sale_price',true) > 0 ){
                                                                        $show .= '<span style="float: right;margin: 10px 0px 0px 0px;"><b><em>'. __('Precio rebajado establecido','wc-total-shop').': '.esc_attr( get_post_meta($ids,'_sale_price',true)).' '.get_woocommerce_currency_symbol().'  </em></b> </span>';
                                                                    }
                                                                    $show .='</div></div>';
                                            }
                                                $show .='</div><!--aqui termina class="accordion"-->';
                                                $show .=    '<div class="all_variaciones_div">
                                                                    <table class="form-table">
                                                                        <tr>
                                                                            <th scope="row" ><span>'.__('Precio normal','wc-total-shop').'('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input   id="'.str_replace("-", "_", $role).'_'.$id.'_todas_p_n" name="'.str_replace("-", "_", $role).'_'.$id.'_todas_p_n"
                                                                            min="0" step="0.001"  type="number" value="'.self::cargaDatos($id, str_replace("-", "_", $role).'_'.$id.'_todas_p_n').'" />
                                                                            <span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row" ><span>'.__('Precio rebajado','wc-total-shop').'('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input  id="'.str_replace("-", "_", $role).'_'.$id.'_todas_p_r" name="'.str_replace("-", "_", $role).'_'.$id.'_todas_p_r"
                                                                            min="0" step="0.001"  type="number" value="'.self::cargaDatos($id, str_replace("-", "_", $role).'_'.$id.'_todas_p_r').'" />
                                                                            <span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                    </table>
                                                           </div>';
                                                          // echo $show;
                                    }elseif ($product_type == 'simple'){
                                                $show .='<div >';   
                                                $show .= '<div >
                                                                <div>
                                                                    <table class="form-table">
                                                                        <tr>
                                                                            <th scope="row" ><span>'. __('Precio normal','wc-total-shop').' ('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input id="'.str_replace("-", "_", $role).'_'.$id.'_retail" name="'.str_replace("-", "_", $role).'_'.$id.'_retail" min="0" step="0.001"  type="number" value="'.self::cargaDatos($id,str_replace("-", "_", $role).'_'.$id.'_retail').'" /><span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row" ><span>'.__('Precio rebajado','wc-total-shop').' ('.get_woocommerce_currency_symbol().'): </span></th>
                                                                            <td><input id="'.str_replace("-", "_", $role).'_'.$id.'_sale" name="'.str_replace("-", "_", $role).'_'.$id.'_sale" min="0" step="0.001"  type="number" value="'.self::cargaDatos($id,str_replace("-", "_", $role).'_'.$id.'_sale').'" /><span>'.get_woocommerce_currency_symbol().'</span></td>
                                                                        </tr>
                                                                    </table>
                                                                    <span style="float: left;margin: 10px 0px 0px 0px;"><b><em>'. __(' Precio Normal establecido','wc-total-shop').': '.esc_attr( get_post_meta($prod->id,'_regular_price',true)).' '.get_woocommerce_currency_symbol().' </em></b>  </span>';
                                                                    if( get_post_meta($prod->id,'_sale_price',true) > 0 ){
                                                                        $show .= '<span style="float: right;margin: 10px 0px 0px 0px;"><b><em>'. __('Precio rebajado establecido','wc-total-shop').': '.esc_attr( get_post_meta($prod->id,'_sale_price',true)).' '.get_woocommerce_currency_symbol().'  </em></b> </span>';
                                                                    }
                                                                    $show.='</div></div>';
                                                $show .='</div>';
                                              //  echo $show;
                                    }
                        $show .='</div>';
                            $concidencias++;
                        }
                    }
                }
            $show .='<input type="hidden" name="product_id" value="'.$id.'">
                         <input type="hidden" name="wcst_pp_gg_nonce" value="'.wp_create_nonce('wcts_g_p').'" />
                         <hr />
                         <input style="float: right;margin-bottom: 10px;" type="button" id="wcts_meta_price_save" class="button button-primary button-large" value="'.__('Guardar Cambios','wc-total-shop').'" />
                        </div>';
            return $show;
    }
  
  
  }
  
  endif;
  
 return WC_total_metabox_product::getInstance();
 
