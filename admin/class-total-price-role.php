<?php
/**
 * Wc Total Shop Fixed Price by Role - Core Class
 *
 * @version 0.8.9
 * @since   0.8.9
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_total_fixed_price_role' ) ) :
  
include_once( plugin_dir_path( __FILE__ ) . 'views/metabox_product.php' );
 
 class WC_total_fixed_price_role{
    
    function __construct(){
            add_action('add_meta_boxes', array(__CLASS__, 'roles_precios_fijos'));
            add_action( 'admin_print_styles-post-new.php', array( __CLASS__, 'encolar_fs'      ) );
            add_action( 'admin_print_styles-post.php',     array( __CLASS__, 'encolar_fs'      ) );
            add_action( 'wp_ajax_wcts_g_p', array(__CLASS__,'guardaDatos'));
            // Precios
            add_filter( 'woocommerce_product_get_price', array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            add_filter( 'woocommerce_product_get_sale_price',array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            add_filter( 'woocommerce_product_get_regular_price',array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            
            //add_filter( 'woocommerce_cart_item_price', array(__CLASS__, 'precios_carrito_min'), 10, 3 );
            add_filter( 'woocommerce_cart_item_price_html', array(__CLASS__, 'precios_carrito_min'), 10, 3 );
            add_filter( 'woocommerce_before_calculate_totals', array(__CLASS__, 'change_product_price_cart'), 10, 1 );

            // Variaciones
            add_filter( 'woocommerce_get_variation_price', array ( __CLASS__, 'cambiar_precio_por_role' ), 10, 2);
            add_filter( 'woocommerce_variation_prices_price',array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            add_filter( 'woocommerce_variation_prices_regular_price',  array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            add_filter( 'woocommerce_variation_prices_sale_price',array(__CLASS__, 'cambiar_precio_por_role' ), 99, 2 );
            add_filter('woocommerce_get_variation_prices_hash', array ( __CLASS__, 'woocommerce_get_variation_prices_hash' ), 10, 3);
            
            // filter <del> tags for variable products
            add_filter('woocommerce_variable_sale_price_html', array ( __CLASS__, 'woocommerce_variable_sale_price_html' ), 10, 2 );
        }

        
        
         public static function change_product_price_cart( $cart_object ) {

            if ( is_admin() && ! defined( 'DOING_AJAX' ) )
                return;

            foreach ( $cart_object->get_cart() as $cart_item ) {
                
                     $product =   wc_get_product($cart_item['variation_id']);
                    // Set your price
                    $price = self::cambiar_precio_por_role($product->get_price(), $product);
                    

                    // WooCommerce versions compatibility
                    if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
                        $cart_item['data']->price = $price; // Before WC 3.0
                    } else {
                        $cart_item['data']->set_price( $price ); // WC 3.0+
                    }
                
            }
        }
        
  
        
    public static function precios_carrito_min( $html, $cart_item, $cart_item_key ) {
         $product =   wc_get_product($cart_item['variation_id']);
         $price = self::cambiar_precio_por_role($product->get_price(), $product);
          
        $custom_price_html = "<b>".wc_price($price )."</b>"; // just an example of HTML
        return $custom_price_html;
    }
    
        
        
     public static function roles_precios_fijos($post){
            
            add_meta_box('_wcts_fixed_price_roles',__('Roles disponibles para precios fijos','wc-total-shop'),array(__CLASS__,'agregrar_role'),'product', 'normal', 'high');
            
        }

     public static function agregrar_role($post){
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-pr-fj');
            $concidencias=0;
            
            if(is_object($post)){ 
                $id = $post->ID;
            } else {
                $id = $post;
            }
            $prod = wc_get_product($id);
            
            $product_type = ($terms = wp_get_object_terms( $id, 'product_type' ))  ?  sanitize_title( current( $terms )->name ) : apply_filters( 'default_product_type', 'simple' );
           
           ?>
             <div id="wcts-tabs-div-form" method="POST" action="<?php echo admin_url('admin-ajax.php?action=wcts_g_p'); ?>" > 
                    <?php 
                       echo WC_total_metabox_product::getInstance()->get_form_prices($role_checklist, $id, $prod, $product_type);
                    ?>
            </div>
            <?php
            
    }

     public static function encolar_fs() {
		$color = get_user_meta( get_current_user_id(), 'admin_color', true );
		wp_enqueue_style(  'tab-metabox-wcst', plugins_url( '../assets/css/wcstt.css',   __FILE__ )                    );
		wp_enqueue_style(  "tab-metabox-wcst-$color",       plugins_url( "../assets/css/wcstt-$color.css", __FILE__ )                    );
		wp_enqueue_script( 'tab-metabox-wcst', plugins_url( '../assets/js/wcstt.js',    __FILE__ ), array( 'jquery' ) );
	}

     public static function guardaDatos(){
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-pr-fj');
        // Stop the script when doing autosave
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
        
        if(isset($_POST['product_id'])){
            $post_id = $_POST['product_id'];
        }else {
            wp_send_json_error('Muerte 2: No AUTORIZADO, !!LLAMEN AL 911¡¡');
            die(-1);
        }
        
            $id= $post_id ;
            
            $product_type = ($terms = wp_get_object_terms( $id, 'product_type' ))  ?  sanitize_title( current( $terms )->name ) : apply_filters( 'default_product_type', 'simple' );
        
        if(!(wp_verify_nonce( $_POST['wcst_pp_gg_nonce'], 'wcts_g_p') && is_admin())) {
            wp_send_json_error('<h2 style="color: red;font-weight: 700;">Muerte 1: No te haz logeado, o tratas de entrar sin autorizacion</h2>');
            die(-1);
        }  
        
        if ($role_checklist) {
                $concidencias=0;
                    foreach (get_editable_roles() as $role => $info) {
                        if (in_array($role, $role_checklist)) {
                                if($product_type == 'variable'){
                                
                                            $nombre_c_t = str_replace("-", "_", $role).'_'.$id.'_todas_check_b';
                                            $nombre_c_t_v = isset($_POST[$nombre_c_t]) ? sanitize_text_field($_POST[$nombre_c_t]) : '';
                                            
                                            $nombre_pn_t = str_replace("-", "_", $role).'_'.$id.'_todas_p_n'; 
                                            $nombre_pn_t_v = isset($_POST[$nombre_pn_t]) ? floatval($_POST[$nombre_pn_t]): '';
                                            
                                            $nombre_pr_t  = str_replace("-", "_", $role).'_'.$id.'_todas_p_r';
                                            $nombre_pr_t_v  = isset($_POST[$nombre_pr_t]) ? floatval($_POST[$nombre_pr_t]): '';
                                            
                                                if(isset($nombre_c_t_v) && $nombre_c_t_v === 'checked' ){
                                                        update_post_meta($id,'wcts_'.$nombre_c_t , $nombre_c_t_v);
                                                        if( (!empty($nombre_pn_t_v ) || !empty($nombre_pr_t_v )) && ($nombre_pn_t_v > 0  || $nombre_pr_t_v >0 ))  {
                                                            ($nombre_pn_t_v >0) ? update_post_meta($id,'wcts_'.$nombre_pn_t , esc_attr($nombre_pn_t_v ) ) : delete_post_meta($id,'wcts_'.$nombre_pn_t); 
                                                            ($nombre_pr_t_v >0) ? update_post_meta($id,'wcts_'.$nombre_pr_t , esc_attr( $nombre_pr_t_v ) ) : delete_post_meta($id,'wcts_'.$nombre_pr_t); 
                                                        }else {
                                                            delete_post_meta($id,'wcts_'.$nombre_c_t);
                                                            delete_post_meta($id,'wcts_'.$nombre_pn_t);
                                                            delete_post_meta($id,'wcts_'.$nombre_pr_t);
                                                        }
                                                    }else {
                                                        delete_post_meta($id,'wcts_'.$nombre_c_t);
                                                        delete_post_meta($id,'wcts_'.$nombre_pn_t);
                                                        delete_post_meta($id,'wcts_'.$nombre_pr_t);
                                                    }
                            
                                            
                                            $args = array(
                                                'post_type'      => 'product_variation',
                                                'post_status'    => array( 'private', 'publish' ),
                                                'posts_per_page' => -1, 
                                                'orderby'        => array( 'menu_order' => 'ASC', 'ID' => 'DESC' ),
                                                'post_parent'    => $id,
                                                'fields' => 'ids'
                                            );
                                            
                                            $variations = get_children( $args );
            
                                            foreach($variations as $ids){
                                                 
                                                $nombre_n = str_replace("-", "_", $role).'_'.$ids.'_retail';
                                                $nombre_n_v =  isset($_POST[$nombre_n]) ? floatval($_POST[$nombre_n]) : '';
                                                
                                                $nombre_r = str_replace("-", "_", $role).'_'.$ids.'_sale';
                                                $nombre_r_v = isset($_POST[$nombre_r]) ? floatval($_POST[$nombre_r]) : '';
                                                
                                                if(isset($nombre_c_t_v) && $nombre_c_t_v === 'checked' ){
                                                    if( (!empty($nombre_pn_t_v ) || !empty($nombre_pr_t_v )) && ($nombre_pn_t_v >0  || $nombre_pr_t_v >0 ))  {
                                                        ($nombre_pn_t_v >0) ? update_post_meta($ids,'wcts_'.$nombre_n , esc_attr( $nombre_pn_t_v ) ) : delete_post_meta($ids,'wcts_'.$nombre_n ) ;
                                                        ($nombre_pr_t_v >0) ? update_post_meta($ids,'wcts_'.$nombre_r , esc_attr($nombre_pr_t_v ) ) : delete_post_meta($ids,'wcts_'.$nombre_r)  ;
                                                    }elseif(!($nombre_pn_t_v >0  || $nombre_pr_t_v >0 )) {
                                                        (!($nombre_pn_t_v >0)) ? delete_post_meta($ids,'wcts_'.$nombre_n ) : '';
                                                        (!($nombre_pr_t_v >0)) ?  delete_post_meta($ids,'wcts_'.$nombre_r) : '' ;
                                                    }
                                                }elseif( (!empty($nombre_n_v) || !empty($nombre_r_v)) && ($nombre_n_v > 0 || $nombre_r_v > 0) ) {
                                                        ($nombre_n_v > 0) ? update_post_meta( $ids,'wcts_'.$nombre_n ,esc_attr( $nombre_n_v ) ) : delete_post_meta($ids,'wcts_'.$nombre_n) ;
                                                        ($nombre_r_v > 0) ? update_post_meta( $ids,'wcts_'.$nombre_r ,esc_attr( $nombre_r_v ) ) : delete_post_meta($ids,'wcts_'.$nombre_r) ;
                                                }
                                             }
                                    }elseif ($product_type == 'simple'){
                                                $nombre_n = str_replace("-", "_", $role).'_'.$id.'_retail';
                                                $nombre_n_v =  isset($_POST[$nombre_n]) ? floatval($_POST[$nombre_n]) : '';
                                                
                                                $nombre_r = str_replace("-", "_", $role).'_'.$id.'_sale';
                                                $nombre_r_v = isset($_POST[$nombre_r]) ? floatval($_POST[$nombre_r]) : '';
                                                
                                                if( (!empty($nombre_n_v) || !empty($nombre_r_v)) && ($nombre_n_v > 0 || $nombre_r_v > 0) ) {
                                                        ($nombre_n_v > 0) ? update_post_meta( $id,'wcts_'.$nombre_n ,esc_attr( $nombre_n_v ) ) : delete_post_meta($id,'wcts_'.$nombre_n) ;
                                                        ($nombre_r_v > 0) ? update_post_meta( $id,'wcts_'.$nombre_r ,esc_attr( $nombre_r_v ) ) : delete_post_meta($id,'wcts_'.$nombre_r) ;
                                                }else {
                                                        delete_post_meta($id,'wcts_'.$nombre_n ) ;
                                                        delete_post_meta($id,'wcts_'.$nombre_r)  ;
                                                }
                                    }
                             $concidencias++;
                        }
                    }
                }
                wp_send_json_success(WC_total_metabox_product::getInstance()->get_form_prices($role_checklist,$id, wc_get_product($id), $product_type)); 
                wp_die(); 
    }
	
    public static function cambiar_precio_por_role($price, $_product){
         global $post, $woocommerce;
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-pr-fj');
            
        if ( ($post == null) || !is_admin() ) {
          $id_prt = ( isset( $_product->variation_id ) ) ? $_product->variation_id : $_product->id;
          
          $product_type = ($terms = wp_get_object_terms( $id_prt, 'product_type' ))  ?  sanitize_title( current( $terms )->name ) : apply_filters( 'default_product_type', 'simple' );
            
          if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                       $nombre_n = str_replace("-", "_", $role).'_'.$id_prt.'_retail';
                       $nombre_r = str_replace("-", "_", $role).'_'.$id_prt.'_sale';
                        
                       if ('' !=esc_attr(get_post_meta( $id_prt,'wcts_'.$nombre_n, true )) || ''!=esc_attr(get_post_meta( $id_prt,'wcts_'.$nombre_r, true )) ){
                           return self::realPrice($price, $id_prt, $nombre_n, $nombre_r, $_product,current_filter());
                       }elseif ($product_type == 'variable'){
                            $id_prt = self::getHijos($id_prt, $role);
                            $nombre_n = str_replace("-", "_", $role).'_'.$id_prt.'_retail';
                       	    $nombre_r = str_replace("-", "_", $role).'_'.$id_prt.'_sale';
                            return self::realPrice($price, $id_prt, $nombre_n, $nombre_r, $_product,current_filter());
                        }
                    }               
                }
            }
            
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-dis');
             if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        return self::realPrice_porcentaje($price, $_product, current_filter());
                    }               
                }
            }
        }
        return $price;
	}
	
	private static function getHijos($id, $role){
        $product = wc_get_product($id);
        foreach ($product->get_children() as $hijo) {
            if(!empty($hijo) && $hijo > 0 && ('' !=esc_attr(get_post_meta( $hijo,'wcts_'. str_replace("-", "_", $role).'_'.$hijo.'_retail', true )) || ''!=esc_attr(get_post_meta( $hijo,'wcts_'.str_replace("-", "_", $role).'_'.$hijo.'_sale', true )))){
                return $hijo;
                break;
            }
        }
        return $id;
	}
	
	private static function realPrice_porcentaje($price,$_product, $filter_actual){
         $discount = (WCTS_Deserializer::getInstance()->get_value("discount-pocentaje"))/100;
         
                            $regular_price_per_product = $discount >0  ? ($price - ($price * $discount)) : $price;
                            if ( 'woocommerce_product_get_price_including_tax' == $filter_actual || 'woocommerce_product_get_price_excluding_tax' == $filter_actual ) {
                                $get_price_method = 'get_price_' .esc_attr(WCTS_Deserializer::getInstance()->get_value( 'woocommerce_tax_display_shop' )).'uding_tax';
                                return $_product->$get_price_method();
                            }elseif ( 'woocommerce_product_get_price' == $filter_actual || 'woocommerce_variation_prices_price' == $filter_actual  ) {
                                $sale_price_per_product =$price;
                                return ( '' != $sale_price_per_product && $sale_price_per_product < $regular_price_per_product ) ? $sale_price_per_product : $regular_price_per_product;
                            } elseif ( 'woocommerce_product_get_regular_price' == $filter_actual || 'woocommerce_get_variation_price' == $filter_actual ) {
                                return $regular_price_per_product;
                            } elseif ( 'woocommerce_product_get_sale_price' == $filter_actual || 'woocommerce_variation_prices_sale_price' == $filter_actual ) {
                                $sale_price_per_product =$price;
                                return ( '' != $sale_price_per_product ) ? $sale_price_per_product : $price;
                            }elseif ( 'woocommerce_before_calculate_totals' == $filter_actual  ) {
                                $sale_price_per_product = esc_attr(get_post_meta( $producID,'wcts_'.$nombre_r, true ));
                                return ( '' != $sale_price_per_product && $sale_price_per_product < $regular_price_per_product ) ? $sale_price_per_product : $regular_price_per_product;
                            }
         return $price;
	}
	
	private static function realPrice($price, $producID, $nombre_n, $nombre_r, $_product, $filter_actual){
         
                            $regular_price_per_product = esc_attr(get_post_meta( $producID,'wcts_'.$nombre_n, true )) >0  ? esc_attr(get_post_meta( $producID,'wcts_'.$nombre_n, true )) : $price;
                            if ( 'woocommerce_product_get_price_including_tax' == $filter_actual || 'woocommerce_product_get_price_excluding_tax' == $filter_actual ) {
                                $get_price_method = 'get_price_' .esc_attr(WCTS_Deserializer::getInstance()->get_value( 'woocommerce_tax_display_shop' )).'uding_tax';
                                return $_product->$get_price_method();
                            }elseif ( 'woocommerce_product_get_price' == $filter_actual || 'woocommerce_variation_prices_price' == $filter_actual  ) {
                                $sale_price_per_product = esc_attr(get_post_meta( $producID,'wcts_'.$nombre_r, true ));
                                return ( '' != $sale_price_per_product && $sale_price_per_product < $regular_price_per_product ) ? $sale_price_per_product : $regular_price_per_product;
                            } elseif ( 'woocommerce_product_get_regular_price' == $filter_actual || 'woocommerce_get_variation_price' == $filter_actual ) {
                                return $regular_price_per_product;
                            } elseif ( 'woocommerce_product_get_sale_price' == $filter_actual || 'woocommerce_variation_prices_sale_price' == $filter_actual ) {
                                $sale_price_per_product =esc_attr( get_post_meta( $producID,'wcts_'.$nombre_r, true ));
                                return ( '' != $sale_price_per_product ) ? $sale_price_per_product : $price;
                            }elseif ( 'woocommerce_before_calculate_totals' == $filter_actual || 'woocommerce_cart_item_price_html' == $filter_actual   ) {
                                $sale_price_per_product = esc_attr(get_post_meta( $producID,'wcts_'.$nombre_r, true ));
                                return ( '' != $sale_price_per_product && $sale_price_per_product < $regular_price_per_product ) ? $sale_price_per_product : $regular_price_per_product;
                            }
         return $price;
	}
	
	public static function woocommerce_get_variation_prices_hash ( $hash, $product, $display ) {
	
		// Delete the cached data, if it exists
		$cache_key = 'wc_var_prices' . substr( md5( json_encode( $hash ) ), 0, 22 ) . WC_Cache_Helper::get_transient_version( 'product' );

		delete_transient($cache_key);

		// woocommerce 2.5.1
		delete_transient('wc_var_prices_' . $product->id);

		return $hash;
	}
	
	public static function woocommerce_variable_sale_price_html ( $pricehtml, $product ) {
            $string = $pricehtml;

            global $post, $woocommerce;
            
            if(is_object($post)){ 
                $id = $post->ID;
            } else {
                $id = $post;
            }
            $prod = wc_get_product($id);
            
            if ( ($post == null) || !is_admin() ) {
                $commission = self::cambiar_precio_por_role( $prod->get_sale_price(), $product );
                if ( $commission ) {
                    $string=preg_replace('/<del[^>]*>.+?<\/del>/i', '', $string);
                }
            }
            return $string;
        }
    
}
endif;

return new WC_total_fixed_price_role();

 
 
