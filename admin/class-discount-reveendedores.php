<?php
/**
 * WC Total Shop Core Class
 *
 * @version 0.8.9.1
 * @since   0.8.9.1
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_REVRE_Discount' ) ) :
 
 class WC_REVRE_Discount{
    
     function __construct(){
		add_action('woocommerce_product_write_panel_tabs', array(__CLASS__,'woocommerce_product_write_panel_tabs') );
		add_action('woocommerce_product_write_panels', array(__CLASS__,'woocommerce_product_write_panels') ); 
		//add_action( 'wp_ajax_wcts_disco_rev', array(__CLASS__,'guardarDescuento'));
		add_action( 'save_post', array(__CLASS__,'guardarDescuento'));
         }
    
	public static function woocommerce_product_write_panel_tabs() {
		echo '<li class="general_options"><a href="#discount_especial_rev">' . __( 'Descuento Esp. Rev.', 'wc-total-shop' ) . '</a></li>';
	}
	
	private static function cargaDatos($prod_id, $nombre){
            $post_id_ = get_post_custom($prod_id);
            $dato = isset($post_id_['wcts_'.$nombre][0]) ? esc_attr($post_id_['wcts_'.$nombre][0]) : null;
            
            return $dato;
        }
	
	public static function woocommerce_product_write_panels() {
		global $post;
		$product = get_product( $post->ID );
		
		$valor_porc = self::cargaDatos($product->id, $product->id.'_discount_rev');
		
		?>
		<div id="discount_especial_rev" class="panel woocommerce_options_panel">
			<div class="options_group">
				<h2 class="title"> Descuento especiales para revendedores</h2>
				<p class="description">
					<?php echo __( 'Selecciona el porcentaje por el cual se hara descuento especial al precio de revendedores .', 'wc-total-shop' ); ?>
				</p>
			</div>

			<div class="options_group custom_tab_options">
			<p class="form-field">
 				<label for="<?php echo $product->id; ?>_discount_rev" >Descuento: </label>
				<select name="<?php echo $product->id; ?>_discount_rev" id="<?php echo $product->id; ?>_discount_rev"  class="select short">
					<option value="">Elige Porcentaje</option>
					<option value="10" <?php if($valor_porc==10){echo 'selected="selected"';} ?> >10%</option>
					<option value="15" <?php if($valor_porc==15){echo 'selected="selected"';} ?>  >15%</option>
					<option value="20" <?php if($valor_porc==20){echo 'selected="selected"';} ?>  >20%</option>
					<option value="30" <?php if($valor_porc==30){echo 'selected="selected"';} ?>  >30%</option> 
				</select>
			</p>
			</div>
			<?php 
			/*
			method="POST" action="<?php echo admin_url('admin-ajax.php?action=wcts_disco_rev'); ?>"
			<input type="hidden" name="wcst_disc_rre_nonce" value="/*<?php wp_create_nonce('wcts_disco_rev') ?>" /> 
			<input style="float: right;margin: 10px;" type="button" id="wcts_disc_rev_save" class="button button-primary button-large" value="<?php _e('Guardar Cambios','wc-total-shop') ?>" /> 
			*/
			?>
		</div>
	<?php
	}
	
	public static function guardarDescuento($post_id){

		$product = get_product( $post_id );
		
		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
		
		$disc_re_tag = $product->id.'_discount_rev';
                $disc_re_v = isset($_POST[$disc_re_tag]) ? floatval($_POST[$disc_re_tag]): '';
                
                 if(isset($disc_re_v) && $disc_re_v !=''){
	              update_post_meta($product->id,'wcts_'.$disc_re_tag , $disc_re_v);
	          }else {
	              delete_post_meta($product->id,'wcts_'.$disc_re_tag);
	          }
	          
	         /*
	          if(!(wp_verify_nonce( $_POST['wcst_disc_rre_nonce'], 'wcts_disco_rev') && is_admin())) {
	            wp_send_json_error('<h2 style="color: red;font-weight: 700;">Muerte 1: No te haz logeado, o tratas de entrar sin autorizacion</h2>');
	            die(-1);
	        } 
	        */ 
	       	//wp_send_json_success(WC_total_metabox_product::getInstance()->get_form_prices($role_checklist,$id, wc_get_product($id), $product_type)); 
		//wp_die();
        }
	
  }
  
  
endif;

return new WC_REVRE_Discount();

 
 
