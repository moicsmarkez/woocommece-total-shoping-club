<?php
/**
 * WC Total ShopMinimun Order Amount for WooCommerce - Core Class
 *
 * @version 0.8.9
 * @since   0.8.9
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_minimum_order_amount' ) ) :
 
 class WC_minimum_order_amount {
    
     function __construct(){
        
            add_action( 'woocommerce_checkout_process', array( __CLASS__, 'minimum_order_amount' ));
            add_action( 'woocommerce_before_cart' , array( __CLASS__, 'minimum_order_amount' ));
            add_action( 'woocommerce_after_cart' , array( __CLASS__, 'minimum_order_amount' ));
         }
    
    
     public static function minimum_order_amount() {
     
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-min');
            $minimum = WCTS_Deserializer::getInstance()->get_value('minimiun-amount');
            $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            
            if ( (WC()->cart->subtotal_ex_tax < $minimum) && ($concidencias > 0)) {
                if( is_cart() ) {
                    wc_add_notice( 
                        sprintf( 'Debe tener un pedido con un mínimo de %s para hacer su compra, su total de la orden actual es  %s.' , 
                            wc_price( $minimum ), 
                            wc_price( WC()->cart->subtotal_ex_tax )
                        ), 'error' 
                    );
                } else {
                    wc_add_notice( 
                        sprintf( 'Debe tener un pedido con un mínimo de %s para hacer su compra, su total de la orden actual es  %s.' ,  
                            wc_price( $minimum ), 
                            wc_price( WC()->cart->subtotal_ex_tax )
                        ), 'error' 
                    );
                }
            }
        }
  }
  
  
endif;

return new WC_minimum_order_amount();

 
 
