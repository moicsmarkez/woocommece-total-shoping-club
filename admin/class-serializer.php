<?php
/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * @package Wc_Total_Shop 
 */

/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * This will also check the specified nonce and verfy that the current user has
 * permission to save the data.
 *
 * @package Wc_Total_Shop 
 */
 
 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'WCTS_Serializer' ) ) :

class WCTS_Serializer {

    public $errors = array();
    public $messages_update = array();

	/**
	 * Initializes the funtion by registering the save function with the
	 * admin_post hook so that we can save our options to the database.
	 */
	public function __construct() {
		add_action( 'admin_post', array( $this, 'save' ) );
        add_action('get_wcts_settings_messages', array( $this, 'get_messages' ) );
	}

	/**
	 * Validates the incoming nonce value, verifies the current user has
	 * permission to save the value from the options page and saves the
	 * option to the database.
	 */
	public function save() {

		// First, validate the nonce and verify the user as permission to save.
		if ( ! ( $this->es_valido_once() && current_user_can( 'manage_options' ) ) ) {
			// TODO: Display an error message.
 			$this->add_wcts_message('You do not have permission for this operation.', 'error');
		}else {
		
            $options = array(
	            'minimiun-amount',
                'discount-pocentaje',
                're-tax-pocentaje',
                'tax-input-role-min',
                'tax-input-role-dis',
                'tax-input-role-re-tax',
                'tax-input-role-pr-fj',
                'tax-input-role-cif',
                'tax-input-role-exp',
            );
    
            // If the above are valid, sanitize and save the option.
            foreach ($options as $opt){
                $opt1 = wp_unslash($_REQUEST[$opt] );
                if (!empty($opt1)) {
                    update_option( $opt, $this->wcts_general_sanitize($opt1));
                }
            }
            
 		 $this->add_wcts_message( __('Opciones Guardadas con Exito!.','wc-total-shop'), 'update');
		}

		$this->redirect();
		
	

	}

     private static function wcts_general_sanitize($input){
                if(!empty($input) && isset($input)){
                    $input  = !(is_array($input)) ? sanitize_text_field($input) : array_map( 'sanitize_text_field', wp_unslash($input));
                }
                return $input ;
            }
            
     
	/**
	 * Determines if the nonce variable associated with the options page is set
	 * and is valid.
	 *
	 * @access private
	 *
	 * @return boolean False if the field isn't set or the nonce value is invalid;
	 *                 otherwise, true.
	 */
	private function es_valido_once() {

		// If the field isn't even in the $_POST, then it's invalid.
		if ( ! isset( $_POST['wcts-prior-shop'] ) ) { // Input var okay.
			return false;
		}

		$field  = wp_unslash( $_POST['wcts-prior-shop'] );
		$action = 'wcts-settings-save';

		return wp_verify_nonce( $field, $action );

	}

	/**
	 * Redirect to the page from which we came (which should always be the
	 * admin page. If the referred isn't set, then we redirect the user to
	 * the login page.
	 *
	 * @access private
	 */
	private function redirect() {

		// To make the Coding Standards happy, we have to initialize this.
		if ( ! isset( $_POST['_wp_http_referer'] ) ) { // Input var okay.
			$_POST['_wp_http_referer'] = wp_login_url();
		}

		// Sanitize the value of the $_POST collection for the Coding Standards.
		$url = sanitize_text_field(
				wp_unslash( $_POST['_wp_http_referer'] ) // Input var okay.
		);

		// Finally, redirect back to the admin page.
		wp_safe_redirect( urldecode( $url ) );

        exit;

	}
	
	/**
 * Add an error message to the error message collection. If the incoming
 * message is an array, then an unordered list of errors will be rendered.
 *
 * @param array  $message The message to add to the collection of errors.
 */
    public function add_wcts_message( $message, $type ) {

        // Add the message to the array of errors and then update the option.
        if($type === 'error'){
            $this->errors[] = esc_html( $message );
        }elseif($type === 'update'){
            $this->messages_update[] = esc_html($message);
        }
        $this->serialize_messages();
    }

/**
 * Saves the collection of error messages to the database.
 *
 * @access private
 */
    private function serialize_messages() {
        if(!empty($this->errors)){
            update_option( 'wcts-errors', $this->errors );
        }
        if(!empty($this->messages_update)){
            update_option( 'wcts-msg-update', $this->messages_update);
        }
    }
    
    public function get_messages() {

         $this->get_errors();
         $this->get_msg_updates();
         $this->display_msg_wcts();

    }
    
    private function get_errors() {
        $this->errors = WCTS_Deserializer::getInstance()->get_value( 'wcts-errors' )!=null ? WCTS_Deserializer::getInstance()->get_value( 'wcts-errors' ) : '';
        delete_option( 'wcts-errors' );
    }
    
    private function get_msg_updates() {
        $this->messages_update = WCTS_Deserializer::getInstance()->get_value( 'wcts-msg-update' )!=null ? WCTS_Deserializer::getInstance()->get_value( 'wcts-msg-update' ) : '' ;
        delete_option( 'wcts-msg-update' );
    }
    
    private function display_msg_wcts() {

        if ( !empty( $this->errors ) ) {
             // Create the list of errors.
            $errors = '<ul>';
            foreach ( $this->errors as $error ) {
                $errors .= '<li>' . $error . '</li>';
            }
            $errors .= '</ul>';

            // Create the markup for the error message.
            $html = "
                <div class='notice notice-error'>
                    $errors
                </div><!-- .notice-error -->
            ";

            // Set the HTML we'll allow for sanitization.
            $allowed_html = array(
                'div' => array(
                    'class' => array(),
                ),
                'ul' => array(),
                'li' => array(),
            );

            echo wp_kses( $html, $allowed_html );
        }

        if ( !empty( $this->messages_update ) ) {
             // Create the list of errors.
            $msgs= '<ul>';
            foreach ( $this->messages_update as $msg ) {
                $msgs.= '<li>' . $msg . '</li>';
            }
            $msgs .= '</ul>';

            // Create the markup for the error message.
            $html = "
                <div class='notice notice-success is-dismissible'>
                    $msgs
                </div><!-- .notice-success is-dismissible -->
            ";

            // Set the HTML we'll allow for sanitization.
            $allowed_html = array(
                'div' => array(
                    'class' => array(),
                ),
                'ul' => array(),
                'li' => array(),
            );

            echo wp_kses( $html, $allowed_html );
        }
    }

}

endif;

return new WCTS_Serializer();
