<?php
/**
 * Wc Total Shop Role Recarga de Equivalencia - Core Class
 *
 * @version 0.8.9
 * @since   0.8.9
 * @author  moicsmarkez.
 */

 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'WC_total_re_tax_role' ) ) :
 
 class WC_total_re_tax_role{
    
    function __construct(){
              add_action( 'woocommerce_cart_calculate_fees', array(__CLASS__,'woo_add_cart_fee' ));
        }
    
    public static function woo_add_cart_fee() {
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-re-tax');
            $concidencias = 0;
            
            
            if ( is_admin() && ! defined( 'DOING_AJAX' ) )
                return;

            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            
            if($concidencias > 0 /*&& (get_user_meta(wp_get_current_user()->ID, 'billing_country', true))=='ES'*/){
                $ree = WC()->cart->subtotal_ex_tax  * (( WCTS_Deserializer::getInstance()->get_value('re-tax-pocentaje')/100));

                // add_fee method (TAX will NOT be applied here)
                WC()->cart->add_fee( 'RE '.WCTS_Deserializer::getInstance()->get_value('re-tax-pocentaje').'%', $ree, false );
            }
        }
    
  }
endif;

return new WC_total_re_tax_role();

 
 
