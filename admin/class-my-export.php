<?php
/**
 * WC Total Shop WC_My_Account_Export page for WooCommerce - Core Class
 *
 * @version 0.8.9.1
 * @since   0.8.9.1
 * @author  moicsmarkez.
 */

 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
if ( ! class_exists( 'WC_My_Account_Export' ) ) :
 
 class WC_My_Account_Export{
    
     function __construct(){
                add_filter( 'woocommerce_account_menu_items', array($this,'wcts_account_menu_items'), 10, 1 );
                add_action( 'init', array($this,'wcts_add_my_account_endpoint') );
             //   add_action( 'admin_init', array( $this, 'output_buffer' ) );
                add_action( 'woocommerce_account_csvExport_endpoint', array($this,'wcts_csvExport_endpoint_content'));
             //   add_action( 'init', array( $this, 'output_buffer' ) );
                add_action( 'admin_post_download_stock_csv',array($this,'make_csv_file' ));
                add_action( 'admin_post_nopriv_download_stock_csv', array($this,'make_csv_file' ));
            }
    
            
        
        
        
        public function wcts_account_menu_items( $items ) {
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-exp');
        $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            if($concidencias > 0){
                $items['csvExport'] = __( 'Exportar Csv', 'wc-total-shop' );
            }
           return $items; 
        }
        
        public function wcts_add_my_account_endpoint() {
                add_rewrite_endpoint( 'csvExport', EP_PAGES );
        }
        
        public function wcts_csvExport_endpoint_content() {
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-exp');
        $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            if($concidencias > 0){
                        
                        ?>
                        <form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
			<div style="margin-bottom: 45px;text-align: center;">
                                <h5><?php _e('Descarga nuestro archivo con los datos del almacen. ','wc-total-shop'); ?></h5>
                                <img src="https://s3.amazonaws.com/maunaes/woocommerce/Spectacle.w17942.jpeg"/>
                                    <div>
                                    	<div>
                                           <input type="submit" class="btn btn-warning" value="<?php _e('Descargar Archivo','wc-total-shop'); ?>" />
                                           <input type="hidden" name="action" value="download_stock_csv">
                                    	</div>
                                    </div>
				   <hr />
                                </div>  
                            </div>
                         </form>
                        <?php
            }
        }
        
        public static function wcts_is_endpoint( $endpoint = false ) {
        $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-exp');
        $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            if($concidencias > 0){
                global $wp_query;
                if( !$wp_query )
                    return false;
                return isset( $wp_query->query[ $endpoint ] );
            }else { return false;}
        }
        
        
         public function make_csv_file(){
            ob_start();
            $role_checklist = WCTS_Deserializer::getInstance()->get_value('tax-input-role-exp');
            $concidencias = 0;
            
            
            if ($role_checklist) {
                foreach (get_editable_roles() as $role => $info) {
                    if (in_array($role, $role_checklist) && in_array($role, wp_get_current_user()->roles) ) {
                        $concidencias++; 
                    }               
                }
            }
            if($concidencias > 0){
                    $array_to_csv = array();
                    //Primera Linea
                    $array_to_csv[] = array('reference','Product name','content','images','Stock status','Stock','Regular Price','Sizes','Type'); 
                    
                    $products = $this->get_products_for_export(); 
                    
                    foreach( $products as $item ){ 
                        $product_meta = get_post_meta($item->ID);
                        $item_product = get_product($item->ID);
                        $product_type = $item_product->product_type;
                        
                        $price = floatval($item_product->get_price());
                        
                        $decimal_separator  = wc_get_price_decimal_separator();
                        $thousand_separator = wc_get_price_thousand_separator();
		        $decimals          = wc_get_price_decimals();
                        
                        $price = number_format( $price, $decimals, $decimal_separator, $thousand_separator );
                        
                        $id = $item->ID;
                        
                        if(!empty($item_product->post->post_content)){ $content = strip_tags($item_product->post->post_content); }else{$content = '';};
                        if(!empty($product_meta['_sku'][0])){          $sku = $product_meta['_sku'][0]; }else{ $sku = ''; }
                        $product_name = $item->post_title;
                        if(!empty($product_meta['_stock_status'][0])){ $stock_status = $product_meta['_stock_status'][0]; }else{ $stock_status = ''; }
  			if(!empty($product_meta['_stock'][0])){        $stock = intval($product_meta['_stock'][0]); }else{ $stock = 0; }
                        if($product_type == 'variable'){               $product_type = 'variable'; }else{ $product_type = 'simple'; }
                        
                        if($sku=='Dropshippers'){
                        	continue;
                        }
                        
                        $tallas = array();
                        $image_file_names = array();
                        
                        if ( ( $featured_image_id = get_post_thumbnail_id( $id ) ) && ( $image = wp_get_attachment_image_src( $featured_image_id, 'full' ) ) ) {
                                $image_file_names[] = current( $image );
                            }
                        
                        $images  = isset( $product_meta['_product_image_gallery'][0] ) ? explode( ',', maybe_unserialize( maybe_unserialize( $product_meta['_product_image_gallery'][0] ) ) ) : false;
                        $results = array();

                            if ( $images ) {
                                foreach ( $images as $image_id ) {
                                if ( $featured_image_id == $image_id ) {
                                    continue;
                                }
                                $image = wp_get_attachment_image_src( $image_id, 'full' );
                                if ( $image ) {
                                    $image_file_names[] = current( $image );
                                }
                                }
                            }
                        
                        if($product_type == 'variable'){
                                    $args = array(
                                        'post_parent' => $item->ID,
                                        'post_type'   => 'product_variation', 
                                        'numberposts' => -1,
                                        'post_status' => 'publish' 
                                    ); 
                                    $variations_array = get_children( $args );
                                    foreach($variations_array as $vars){
                                
                                        $var_meta = get_post_meta($vars->ID);
                                        $item_product = get_product($vars->ID);
                        
                                        
                                        foreach($item_product->variation_data as $k => $v){ 
                                            $tag = get_term_by('slug', $v, str_replace('attribute_','',$k));
                                            if($tag == false ){
                                                $tallas[] = $v;
                                            }else{
                                                if(is_array($tag)){
                                                    $tallas[] = $tag['name'].' ';
                                                }else{
                                                    $tallas[] = $tag->name.' ';
                                                }
                                            }
                                        } 
                                        
                                        if(!empty($var_meta['_stock'][0])){ $stock += intval($var_meta['_stock'][0]); }else{ $stock += 0; }
                                            
                                    }
                                   
                            }
                            $array_to_csv[] = array($sku,$product_name, $content, implode( ' | ', $image_file_names ),$stock_status,$stock,$price,implode( ' | ', $tallas ),$product_type);
                        } 
                    $this->export_wcts_convert_to_csv($array_to_csv, 'mauna-es-export.csv', ',');
                }
        }
        
        public function get_products_for_export(){
    
            $args = array();
            $args['post_type'] = 'product';
            $args['posts_per_page'] = -1;
            
            $the_query = new WP_Query( $args );
            
            return $the_query->posts;
        }   
        
        public function export_wcts_convert_to_csv($input_array, $output_file_name, $delimiter){
        
                $temp_memory = fopen('php://memory', 'w');
                
                foreach ($input_array as $line) {
                    fputcsv($temp_memory, $line, $delimiter);
                }
                fseek($temp_memory, 0);
                
                header('Content-Type: application/csv');
                header('Content-Disposition: attachement; filename="' . $output_file_name . '";');
                
                fpassthru($temp_memory);
                exit();
            }

            
        /**
        * Headers allready sent fix
        *
        */        
        public static function output_buffer() {
            ob_start();
        } 
            
  }
  
endif;

return new WC_My_Account_Export();

 
 
